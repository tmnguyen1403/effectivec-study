### Toggle preview ⇧⌘V (Mac)
## Type deduction
1. Template
- `template<typename T>
  void f(ParamType param);
`
- If `void f(const T& param);`
and `int x = 0; f(x);`, then
**T** is `int`, **ParamType** is `const int&`

2. Declare array of the same size
- `template<typename T, std::size_t N>
constexpr std::size_t arraySize(T (&) [N]) noexcept
{ return N;}`
- `int keyVals[] = {1,3,5,7}; 
int mappedVals[arraySize(keyVals)];
std::array<int, arraySize(keyVals)> map2;
`
- using constexpr make result available during compilation

3. Auto
- work like template in most cases
- exception - auto assumes that a braced initializer represents a std::initializer_list
`auto x1 = 27; //type int, value 27
auto x2(27); // ditto
auto x3 = {27}; //type std::initializer_list<int>, value 27
auto x4 {27}; // ditto`
- In November 2014, the C++ Standardization Committee adopted proposal N3922, which eliminates the special type deduction rule for auto and braced initializers using direct initialization syntax, i.e., when there is no “=” preceding the braced initializer (see Item 42). Under N3922 (which isn’t part of C++11 or C++14, but which has been implemented by some compilers), the type of x4 in the examples above is int, not std::initializer_list<int>.
- edge cases [More about auto](codes/auto_edge_cases.cpp)
4. decltype
- [Cases](codes/decltype_examples.cpp)

## Brace initialization
- [Examples for edge cases](codes/brace_init.cpp)

## lvalue & rvalue
- lvalue: can take its address
- rvalue: cannot take its address
- parameter is always lvalue `void f(Widget&& w)` w is lvalue
- copy of lvalues are usually copy constructed
- copy of rvalues are generally `std::move` constructed

## std::move
- `std::move(argument)` - requires only a function argument
- it doesn't move anything
- generate no executable code
- unconditionally casts its argument to an **rvalue**
- don't declare objects `const` if you want to move them
- move requests on `const` objects are silently transformed into copy operations
- Note: avoid using `move` with __universal references__, because that can have the effect of unexpectedly modifying lvalues
- can apply to __the value__ of return by value functions to avoid the expense of making a copy if `move` is supported. DON'T APPLY to local variable of the function since **return value optimization (RV)** already handled such case.

1. BENEFITS:

- A move constructor (like a copy constructor) works by copying the members of another object into itself. The differences of a move constructor are the following:

- The move constructor can be destructive. Since the argument is a non-const reference, it may modify the object passed to it. This is okay since the argument is an rvalue, so the caller does not hold onto a name for the original object, and won’t make use of it (note that its destructor will happen though).

- The move constructor only needs to shallow copy. For example if the object holds a pointer, it doesn’t need to deref the pointer and copy its contents. The move constructor can just copy the pointer’s address. This is where the performance benefits of move constructors happen.

- [Reference](https://www.chromium.org/rvalue-references/#:~:text=std%3A%3Amove()%20will%20cast%20a%20variable%20to%20an%20rvalue.&text=std%3A%3Amove()%20will%20cast%20your%20variable%20to%20an,bind%20to%20a%20move%20constructor.&text=This%20also%20means%20that%20you,uses%20pass%2Dby%2Dvalue.)

## std::forward
- `std::forward<template type>(argument)` - requires both a function argument and a template type argument like `std::string`
- doesn't forward anything
- generate no executable code
- cast to an **rvalue** only if its argument was initialized with an **rvalue**


## Universal reference (T&&)- Item 24
- If a function template parameter has type __T&&__ for a deduced type T, or if an object is declared using __auto&&__, the parameter or object is a universal reference.
- If the form of the type declaration isn’t precisely type&&, or if type deduction does not occur, __type&&__ denotes an rvalue reference.
- Universal references correspond to rvalue references if they’re initialized with rvalues. They correspond to lvalue references if they’re initialized with lvalues. 
- [Examples](./codes/universal_reference_examples.hpp)

## nullptr
- preffer to use other than 0 or NULL
- actual type is std::nullptr_t
- point of all types

## typedef vs using
- `typedef`s don't support templatization, but alias declarations do
- Alias templates avoid the `type` suffix and, in templates, the `typename` prefix often required to refer to `typedef`s
- C++14 offers alias templates for all the C++11 type traits transformations