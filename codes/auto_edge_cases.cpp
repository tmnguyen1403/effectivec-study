/*
std::vector<bool> features(const Widget& w);

Widget w;
…

bool highPriority = features(w)[5];  // is w high priority?
…

processWidget(w, highPriority);      // process w in accord
                                     // with its priority
*/
/*
if change bool to:
    auto highPriority = features(w)[5];
the type will be incorrectly deduced since std::vector::operator[] for bool
returns std::vector<bool>::reference(ref) instead of std::vector<bool>
-this is called a proxy class -->
the calls to features returns a temporary std::vector<bool>, called temp
-->ref(and highPriority) contains a pointer to a word in temp(which is destroyed at the end of the statement)
-->highPriority contains a dangling pointer
-->undefined behavior when calling processWidget(w, highPriority)
*/
