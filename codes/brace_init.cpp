#include <iostream>
#include <vector>

class Widget {
private:
    int number;
    bool flag;
    double score;
public:
    Widget(int i, bool b){
        number = i;
        flag = b;
        std::cout << "Called constructor Widget(int,bool)\n";
    };
    Widget(int i, double d) {
        number = i;
        score = d;
        std::cout << "Called constructor Widget(int,double)\n";

    };
    //Edge case 1 -uncomment to see effect
    // Widget(std::initializer_list<bool> il){
    //     std::cout << "Called constructor Widget(initializer_list<bool>)\n";
    // };

    //Edge case 2 -uncomment to see effect
    // Widget(std::initializer_list<std::string> str){
    //     std::cout << "Called constructor Widget(initializer_list<string>)\n";
    // }

    //Edge case 3
    Widget() { std::cout << "Called default constructor \n"; };
    Widget(std::initializer_list<int> il) { std::cout << "Called constructor Widget(initializer_list<int>)\n";};


};

int main()
{
    /*Edge case 1 -uncomment to see effect
    Widget w{10,5.0}; //this is invalid since std::initializer_list<bool> constructor will be called
    */
    /*Edge case 2 -uncomment to see effect
    --Will not call constructor<std::initializer_list<std::string>> since there is no way to convert 
    int or double to string
    Widget w1(10, true);
    Widget w2{10, true};
    Widget w3(10, 5.0);
    Widget w4{10, 5.0};
    */
    /*Edge case 3 -uncomment to see effect
    
   Widget w1;
    Widget w2 {};
    Widget w4({});
    Widget w5{{}};
    */
   //Edge case 4 - vector
   std::vector<int> v1(5,20);
   std::vector<int> v2{5, 20};

    std::cout <<"elements in v1:\n";
    for (const auto &number : v1) {
        std::cout << number << " ";
    }
    std::cout <<"\nelements in v2 with auto i=0:\n";
    for (auto i = 0; i < v2.size(); ++i) {
        std:: cout << &v2[i] << " " << v2[i] <<"\n";
    }
    std::cout <<"\nelements in v2 with auto &:\n";
    //auto &number: getting the pointer of each element in v2
    //without const, the value can be changed
    for ( auto &number : v2) {
        std:: cout << &number << " " << number <<"\n";
        number = 4;
    }
    std::cout <<"\nelements in v2:\n";
    //auto number: each value in v2 is assigned to number
    //changing the value of number has no side effect
    for (auto number : v2) {
        std:: cout << &number << " " << number <<"\n";
        number = 99;
    }
    std::cout <<"\nelements in v2:\n";
    for (auto number : v2) {
        std:: cout << &number << " " << number <<"\n";
    }
    return 0;
}