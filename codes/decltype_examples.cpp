#include <iostream>
#include <type_traits>
#include <typeinfo>

//All about decltype
int main()
{
    const int i = 0; //decltype(i) is const int
    /*
    if (f(w)) -> bool
    */

   /*
   template<typename Container, typename Index>
   auto authAndAccess(Container& c, Index i) -> decltype(c[i])
   -- RETURNS whatever type operator[] returns
   */
    
    /*C++14
    template<typename Container, typename Index>
    auto authAndAccess(Container & c, Index i)
    {
        authenticateUser();
        return c[i]; // return type deduced from c[i], will strip off "&"
    }
    std::deque<int> d;
    authAndAccess(d,5) = 10; //won't compile since return type is rvalue int
    //Fix
    1.
    decltype(auto) authAndAccess...
    ->return whatever c[i] return
    2.
    decltype(auto) authAndAccess(Container&& c, Index i) {
        ...
        return std::forward<Container>(c)[i];
    }

    decltype almost always yields the type of a variable or expression without any modifications.

For lvalue expressions of type T other than names, decltype always reports a type of T&.

C++14 supports decltype(auto), which, like auto, deduces a type from its initializer, but it performs the type deduction using the decltype rules.
    */
   /*
   To view deduce type, using Boost library
   #include <boost/type_index.hpp>

template<typename T>
void f(const T& param)
{
  using std::cout;
  using boost::typeindex::type_id_with_cvr;

  // show T
  cout << "T =     "
       << type_id_with_cvr<T>().pretty_name()
       << '\n';

  // show param's type
  cout << "param = "
       << type_id_with_cvr<decltype(param)>().pretty_name()
       << '\n';
  …
}
   */
    return 0;
}