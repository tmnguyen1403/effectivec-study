void f(Widget && param); // rvalue reference

Widget&& var1 = Widget(); //rvalue reference

auto&& var2 = var1; //not rvalue reference

template<typename T>
void f(std::vector<T>&& param); //rvalue reference

template<typename T>
void f(T&& param) //not rvalue reference

//******************************************
//initializer
template<typename T>
void f(T&& param);     // param is a universal reference

Widget w;
f(w);                  // lvalue passed to f; param's type is
                       // Widget& (i.e., an lvalue reference)

f(std::move(w));       // rvalue passed to f; param's type is
                       // Widget&& (i.e., an rvalue reference)

//take in any function and check run time
auto timeFuncInvocation =
  [](auto&& func, auto&&... params)               // C++14
  {
    start timer;
    std::forward<decltype(func)>(func)(           // invoke func
      std::forward<decltype(params)>(params)...   // on params
      );                              
    stop timer and record elapsed time;
  };