#include <iostream>
#include <array>

template<typename T, std::size_t N>

constexpr std::size_t arraySize(T (&) [N]) noexcept
{ 
    return N;
}
int main()
{
    std::cout << "hello world" << std::endl;
    int keyVals[] = {1,3,5,7}; 
    int mappedVals[arraySize(keyVals)];
    std::array<int, arraySize(keyVals)> map2;
    for (auto i = 0; i < arraySize(keyVals); ++i)
    {
        mappedVals[i] = keyVals[i] * 2;
        map2[i] = keyVals[i] * 3;
    }
    for (auto i = 0; i < arraySize(keyVals); ++i)
    {
        std::cout << mappedVals[i] << " ";
    }
    std::cout << std::endl;
    for (auto i = 0; i < arraySize(keyVals); ++i)
    {
        std::cout << map2[i] << " ";
    }
}